class AddUserIdAndSiteIdToLike < ActiveRecord::Migration
  def change
    add_column :likes, :user_id, :integer
    add_column :likes, :site_id, :integer
  end
end
