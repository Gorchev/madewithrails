class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :title
      t.string :url
      t.text :description
      t.integer :user_id
      t.integer :category_id

      t.timestamps
    end
  end
end
