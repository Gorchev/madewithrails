class AddHeadquartersToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :headquarters, :string
  end
end
