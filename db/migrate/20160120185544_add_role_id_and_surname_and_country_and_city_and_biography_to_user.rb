class AddRoleIdAndSurnameAndCountryAndCityAndBiographyToUser < ActiveRecord::Migration
  def change
    add_column :users, :role_id, :integer
    add_column :users, :surname, :string
    add_column :users, :country, :string
    add_column :users, :city, :string
    add_column :users, :biography, :text
  end
end
