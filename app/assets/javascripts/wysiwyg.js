//WYSIWYG
function makeBold(){
	document.execCommand("bold", false, null);
}
function makeItalic(){
	document.execCommand("italic", false, null);
}
function makeUnderlined(){
	document.execCommand("underline", false, null);
}
function makeLink(){
	var linkValue = prompt("Type the url")
	document.execCommand("createLink", false, linkValue);
}
function makeUnorderedList() {
	document.execCommand("insertUnorderedList", true);
}
function makeOrderedList() {
	document.execCommand("insertOrderedList", true);
}