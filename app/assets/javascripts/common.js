$(document).ready(function(){
	
	//Add new site
	$(".site .site_new_text").on("click", function(){
		$(this).addClass("active");
	});

	//Open the Account Panel
	$(".resource").on("click", function(){
		$("#mainHeader .account .panel").toggleClass("active");
	});

	//Flash messages
	$(".notice").delay(5000).slideUp();

	//Create a job - location
	$("#job_location_id").change(function(){
		if ($("#job_location_id").val() == 2){
			$(".headquarters_field").show();
		} else {
			$(".headquarters_field").hide();
		}
	});

	//Site - like button
	$(".likes button").click(function(){
		var currentSiteId = $(this).attr("data-site");
		var currentUserId = $(this).attr("data-user");
		$(this).addClass("liked");
		$(this).attr("disabled", true);
		var currentLikes = $(this).parent().find(".count").html();
		var currentLikesToS = parseInt(currentLikes);
		if (currentLikes == 0){
			$(this).parent().find(".count").text(1);
		} else {
			$(this).parent().find(".count").html(currentLikesToS+1);
		}
		$.ajax({
			type: "POST",
			url: "likes/create",
			data: {like: {user_id: currentUserId, site_id: currentSiteId}},
			success: function(){
				console.log("Success");
				$("#topicWall .action").append("<span class='outcome success'>Posted</span>")
			},
			error: function(){
				console.log("Error");
			}
		});
	});

	//New Job / Update Job - parse the content from the div to the text area
	$("#new_job, .edit_job").submit(function(){
		$("#job_description").html($("#contentEditableArea").html());
	});

	//Paste content into New Job / Update Job - strip the tags
	//On paste, strip all html tags
	$(".account_forms").on("paste", "#contentEditableArea", function(){
		setTimeout(function(){
			var currentContent = $("#contentEditableArea").html();
			var div = document.createElement("div");
			div.innerHTML = currentContent;
			$("#contentEditableArea").html(div.innerText || div.textContent);
		}, 10)
	});

	//Showing jobs resume
	$(".jobs .list li").on("click", function(){
		var thisOffset = $(this).position();
		var company = $(this).find(".company").text();
		var title = $(this).find(".title .the_title").text();
		var description = $(this).find(".description").text();
		var url = $(this).find(".url").html();

		$(".jobs .info .arrow").css("top", thisOffset.top);

		$(".jobs .info").find(".title .role").text(title);
		$(".jobs .info").find(".title .company").text(company);
		$(".jobs .info").find(".description").html(description);
		$(".jobs .info").find(".btn").attr("href", url);
	});

	//Developers 
	/*$(".dev_list li").each(function(){
		randomNumber = Math.floor(Math.random() * (150 - 80 + 1)) + 100;
		$(this).find(".image").css({
			width: randomNumber,
			height: randomNumber,
			lineHeight: randomNumber+"px"
		});
		$(this).find(".extra_info").css({
			left: randomNumber + 20 + "px"
		});
	});*/

	$(".dev_list li").on("click", ".close", function(){
		$(".dev_list li").removeClass("disabled");
		$(".dev_list li").removeClass("active");
		$(".dev_list li .close").remove();
		console.log("bo");
	});

	$(".dev_list li .image").on("click", function(){
		$(".dev_list li").addClass("disabled");
		$(".dev_list li").removeClass("active");
		$(".dev_list li .close").remove();
		$(this).parent().addClass("active");
		$(this).parent().prepend("<span class='close'><i class='fa fa-times-circle-o'></i></span>");
		console.log("hey");
	});

	//Show the developer if there is a hash matching their name
	currentDev = window.location.hash;
	if (currentDev != ""){
		$(".dev_list li").addClass("disabled");
		$(".dev_list li"+currentDev).addClass("active");
		$(".dev_list li"+currentDev).prepend("<span class='close'><i class='fa fa-times-circle-o'></i></span>");
	}

	//Show the job if there is a hash matching the job name
	currentJob = window.location.hash;
	console.log(currentJob);
	if (currentJob != ""){
		var thisOffset = $(".jobs .list li"+currentJob).position();
		var company = $(".jobs .list li"+currentJob).find(".company").text();
		var title = $(".jobs .list li"+currentJob).find(".title .the_title").text();
		var description = $(".jobs .list li"+currentJob).find(".description").text();
		var url = $(".jobs .list li"+currentJob).find(".url").html();

		$(".jobs .info .arrow").css("top", thisOffset.top);

		$(".jobs .info").find(".title .role").text(title);
		$(".jobs .info").find(".title .company").text(company);
		$(".jobs .info").find(".description").html(description);
		$(".jobs .info").find(".btn").attr("href", url);
	}

});














