$(document).ready(function(){

	$("#new_site").submit(function(){
		if(checkTitle() & checkUrl() & checkDescription()){
			return true
		} else {
			return false
		}
	});

	$("#post_site .new_site").submit(function(){
		var siteTitle = $("#site_title").val();
		var siteURL = $("#site_url").val();
		var siteCategoryId = $("#site_category_id").val();
		var siteCategoryName = $("#site_category_id option:selected").text();
		var siteDescription = $("#site_description").val();
		var currentNumSites = parseInt($(".counting_sites").text());

		if(checkTitle() & checkUrl() & checkDescription()){

			//Scroll to top
			$("html, body").animate({
				scrollTop: 0
			}, 500);

			//Prepend the site
			$("#sites ul").prepend("<li class='site new_one'><div class='container'><h3><a href='"+siteURL+"'>"+siteTitle+"</a></h3><p class='description'>"+siteDescription+"</p><span class='category category"+siteCategoryId+"'>"+siteCategoryName+"</span><div class='likes'><a class='login_to_like' href='/users/sign_in' title='Login to like'>Login to like</a><span class='count'>0</span></div></div><div class='author'></div></li>");
			//Show the site
			setTimeout(function(){
				$(".new_one").addClass("show");
			}, 500);
			//Increment the total sites
			setTimeout(function(){
				$(".counting_sites").addClass("bounce");
				$(".counting_sites").text(currentNumSites+1);
			}, 900);

			return true
		} else {
			return false
		}
	});

	$(".edit_site").submit(function(){
		if(checkTitle() & checkUrl() & checkDescription()){
			return true
		} else {
			return false
		}
	});

	function checkTitle(){
		if ($("#site_title").val().length < 3){
			if ($("#site_title").parent().find(".error").length == false){
				$("#site_title").parent().append("<span class='error'>Enter site name</span>")
			}
			return false
		} else {
			$("#site_title").parent().find(".error").remove();
			return true
		}
	}

	function checkUrl(){
		var a = $("#site_url").val();
		var regexp = /^[http://]+[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;

		if(regexp.test(a)){
			$("#site_url").parent().find(".error").remove();
			return true
		} else {
			if ($("#site_url").parent().find(".error").length == false){
				$("#site_url").parent().append("<span class='error'>Enter a valid URL</span>");
			}
			return false
		}
	}

	function checkDescription(){
		if ($("#site_description").val().length < 10){
			
				$("#site_description").parent().append("<span class='error'>Enter more than 10 charackters</span>");	
			
			return false
		} else {
			$("#site_description").parent().find(".error").remove();
			return true
		}
	}

	//Count how many charackters left for the site description
	/*$("#site_description").on("keypress", function(e){
		var soFar = $("#site_description").val().length;
		var numCharackters = $(".description_field .counting span").text(95 - soFar);
		var characktersLeft = numCharackters.text();
		$(".description_field .counting span").text(characktersLeft);
		if (characktersLeft == 0){
			$(".description_field .counting span").addClass("red");
			e.preventDefault();
		}
	});*/

});