class Site < ActiveRecord::Base
	validates :title, :url, :description, presence: true

	belongs_to :user
	belongs_to :role
	belongs_to :category
	has_many :likes

	after_create :tweet

	default_scope { order('created_at DESC') }

	def tweet
      client = Twitter::REST::Client.new do |config|
        config.consumer_key        = Rails.application.secrets.twitter_key
        config.consumer_secret     = Rails.application.secrets.twitter_secret
        config.access_token        = Rails.application.secrets.twitter_access_token
        config.access_token_secret = Rails.application.secrets.twitter_access_token_secret
      end
    
      client.update("New website #{self.url} built on ##{self.category.name.downcase.delete(' ')} via @bulitonruby_")
    end
end
