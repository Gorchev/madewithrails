class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessor :subscribe
  before_create :set_default_role

  has_many :sites
  has_many :jobs
  has_many :likes
  belongs_to :role

  #Subscribe User to Mailchimp
  after_create :subscribe_to_list 

  #User avatar
  has_attached_file :avatar, styles: { medium: "600x600>", thumb: "100x100>" }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  def set_default_role
  	self.role ||= Role.find_by_name('registered')
  end

  def subscribe_to_list
    #If the subcribe box is checked, subscribe the User
    if self.subscribe == "1"
      @list_id = Rails.application.secrets.mailchimp_list_id
      gb = Gibbon::Request.new(api_key: Rails.application.secrets.mailchimp_api_key)

      gb.lists(@list_id).members.create(body: {
        email_address: self.email, 
        status: "subscribed", 
        merge_fields: {
          FNAME: self.name, 
          LNAME: self.surname
        }
      })
    end
  end

end
