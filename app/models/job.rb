class Job < ActiveRecord::Base
	belongs_to :user
	belongs_to :location

	after_create :tweet

	def tweet
      client = Twitter::REST::Client.new do |config|
        config.consumer_key        = Rails.application.secrets.twitter_key
        config.consumer_secret     = Rails.application.secrets.twitter_secret
        config.access_token        = Rails.application.secrets.twitter_access_token
        config.access_token_secret = Rails.application.secrets.twitter_access_token_secret
      end
    
      client.update("New #{self.location.name} job - #{self.title} at #{self.company} - http://builtonruby.com/jobs##{self.title.downcase.delete(' ')}at#{self.company.downcase} via @bulitonruby_")
    end
end
