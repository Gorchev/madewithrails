class StaticPagesController < ApplicationController

  before_filter :authenticate_user!, only: :developers 

  def developers
  end

  def user_dashboard
  end

  def user_jobs_dashboard
  end

  def user_sites_dashboard
  end

  def admin_dashboard
  end

end
