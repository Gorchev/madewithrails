class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :configure_permited_paramteters, if: :devise_controller?
  before_filter :global_vars
  
  def configure_permited_paramteters
     devise_parameter_sanitizer.for(:sign_up) << [:name, :surname, :roletitle, :biography, :country, :city, :company, :role_id, :subscribe]
     devise_parameter_sanitizer.for(:account_update) << [:name, :surname, :roletitle, :avatar, :biography, :country, :city, :company, :role_id, :socialmedia_id, :facebook, :twitter, :github, :linkedin]
  end

  private 
    def global_vars
    	@sites_from_user = Site.where(user_id: current_user.id) if user_signed_in?
      @jobs_from_user = Job.where(user_id: current_user.id).order("created_at DESC") if user_signed_in?
      @developers = User.where(role_id: 5)
      @jobs = Job.all.order("created_at DESC")
      @sites = Site.all.order("created_at DESC")
      @users = User.all.order("created_at DESC")
      @locations = Location.all
      @categories = Category.all
    end
end
