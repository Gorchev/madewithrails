class LikesController < ApplicationController
  def new
    @like = current_user.likes.build
  end

  def create
    @like = current_user.likes.build(likes_params)
    if @like.save
	  respond_to do |format|
		  format.js
	  end
    end
  end

  private
    def likes_params
      params.require(:like).permit(:likes, :user_id, :site_id)
    end
end
