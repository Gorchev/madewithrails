class SitesController < ApplicationController

  before_filter :authenticate_user!, except: [:index, :create]

  def index
    @all_sites = Site.order("created_at DESC")
    # if the id params is present
    if params[:id]
      # get all records with id less than 'our last id'
      # and limit the results to 5
      @sites = Site.where('id < ?', params[:id]).limit(5)
    else
      @sites = Site.limit(5)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
  	@site = current_user.sites.build
  end

  def create
    if user_signed_in? #if the user is signed in, create the site the user_id
  	  @site = current_user.sites.build(site_params)
    else  #else just create the site
      @site = Site.new(site_params)
    end
  	if @site.save
      #Redirect to index
      redirect_to(controller: "sites", action: "index")
  	else
  		render("new")
  	end
  end

  def edit
    @site = Site.find(params[:id])
  end

  def update
  	@site = Site.find(params[:id])
  	if @site.update_attributes(site_params)
  		redirect_to(controller: "static_pages", action: "user_sites_dashboard")
      flash[:message] = "Success"
  	else
  		render("edit")
  	end
  end

  def destroy
  	@site = Site.find(params[:id])
  	if @site.destroy
  		redirect_to(controller: "static_pages", action: "user_sites_dashboard")
  	else
  		render("new")
  	end
  end

  private

    def site_params
      params.require(:site).permit(:title, :url, :description, :category_id)
    end

end
