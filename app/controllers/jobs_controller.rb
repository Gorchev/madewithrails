class JobsController < ApplicationController

  before_filter :authenticate_user!, except: :index

  def index
  	@jobs = Job.all.order("created_at DESC")
    @today = Time.now
  end

  def new
  	@job = current_user.jobs.build
  end

  def create
    @job = current_user.jobs.build(jobs_params)
    if @job.save
      redirect_to(action: "index")
    else
      render("new")
    end
  end

  def edit
  	@job = Job.find(params[:id])
  end

  def update
  	@job = Job.find(params[:id])
  	if @job.update_attributes(jobs_params)
  	  redirect_to(controller: "static_pages", action: "user_jobs_dashboard")
    else
      render("new")
    end
  end

  def destroy
    @job = Job.find(params[:id])
    if @job.destroy
      redirect_to(controller: "static_pages", action: "user_jobs_dashboard")
    else
      render("new")
    end
  end

  private
    def jobs_params
      params.require(:job).permit(:title, :url, :location_id, :headquarters, :description, :company)
    end
end
