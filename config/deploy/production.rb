set :stage, :production
server '178.62.73.235', user: 'deploy', roles: %w{web app db}

set :branch, 'master'
set :rails_env, 'production'